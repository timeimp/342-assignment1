//
//  TimerViewController.h
//  342-Assignment1
//
//  Created by timeimp on 1/09/2014.
//  Copyright (c) 2014 James Wilson. All rights reserved.
//

#import <UIKit/UIKit.h>

#include "LTTime.h"


@interface TimerViewController : UIViewController

enum buttonState{
    startMsg,
    stopMsg,
    clearMsg
};

@property (weak, nonatomic) IBOutlet UILabel* timeLabel;

@property (weak, nonatomic) IBOutlet UILabel* bestLabel;
@property (weak, nonatomic) IBOutlet UILabel* worstLabel;

@property (weak, nonatomic) IBOutlet UILabel* averageLabel;
@property (weak, nonatomic) IBOutlet UILabel* currentLabel;

@property (weak, nonatomic) IBOutlet UIButton* timerButton;
@property (weak, nonatomic) IBOutlet UIProgressView *bestProgressBar;
@property (weak, nonatomic) IBOutlet UIProgressView *currentProgressBar;
@property (strong, nonatomic) IBOutlet UIView *worstProgressBar;
@property (weak, nonatomic) IBOutlet UIProgressView *averageProgressBar;

-(IBAction)toggleButton:(id)sender;

-(void)doTick:(NSTimer *)timer;

@end
