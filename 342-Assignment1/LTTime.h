//
//  LTTime.h
//  342-Assignment1
//
//  Created by timeimp on 1/09/2014.
//  Copyright (c) 2014 James Wilson. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LTTime : NSObject

-(NSTimeInterval *)time;
-(NSDate *)dateRecorded;
-(NSString *)comment;

+(NSString *)convertTimeToString:(NSTimeInterval)newTime;


@property (nonatomic) NSString *timeComment;
@property (nonatomic) NSTimeInterval timeValue;
@property (nonatomic) NSDate *timeDate;

@end