//
//  LTModel.h
//  342-Assignment1
//
//  Created by timeimp on 1/09/2014.
//  Copyright (c) 2014 James Wilson. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "LTChallenge.h"

@interface LTModel : NSObject

-(NSArray *)challenges;

-(void)addChallenge: (NSString *) challengeName;

-(LTChallenge*)challengeAtIndex: (NSInteger) index;

-(int)numberOfChallenges;

@property (nonatomic) NSMutableArray* challenges;

@end
