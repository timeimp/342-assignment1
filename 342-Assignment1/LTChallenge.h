//
//  LTChallenge.h
//  342-Assignment1
//
//  Created by timeimp on 1/09/2014.
//  Copyright (c) 2014 James Wilson. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LTChallenge : NSObject

-(NSString *)name;
-(NSArray *)times;

@property (nonatomic) NSString *challengeName;
@property (nonatomic) NSArray *times;

@end
