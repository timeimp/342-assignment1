//
//  LTTime.m
//  342-Assignment1
//
//  Created by timeimp on 1/09/2014.
//  Copyright (c) 2014 James Wilson. All rights reserved.
//

#import "LTTime.h"

@interface LTTime()


@end

@implementation LTTime

-(NSString *)comment
{
    return _timeComment;
}

-(NSTimeInterval *)time
{
    return &(_timeValue);
}

-(NSDate *)dateRecorded
{
    return _timeDate;
}

+(NSString *)convertTimeToString:(NSTimeInterval)newTime
{

    NSUInteger s = (NSUInteger)newTime % 60;

    NSUInteger ms = (newTime * 1000);
    ms = ms % 1000;

    double m = newTime / 60;

    return [NSString stringWithFormat:@"%1.0f:%02ld.%1lu", m, s, (unsigned long)ms];
}

@end
