//
//  ChallengesViewController.m
//  342-Assignment1
//
//  Created by timeimp on 1/09/2014.
//  Copyright (c) 2014 James Wilson. All rights reserved.
//

#import "ChallengesViewController.h"

@interface ChallengesViewController ()



@end

@implementation ChallengesViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [_challengesTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"TimeCell"];

    _lapTimerModel = [[LTModel alloc] init];
    [_lapTimerModel addChallenge: @"Clap 20 Times Challenge"];
    [_lapTimerModel addChallenge: @"Say the Alphabet Challenge"];
    [_lapTimerModel addChallenge: @"00 Meter Sprint Challenge"];
    [_lapTimerModel addChallenge: @"Read the csci342 A1 spec challenge"];

    _challengesTableView.dataSource = self;
    _challengesTableView.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


/* dataSource Implementations */

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TimeCell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier:@"TimeCell"];
    }

    cell.textLabel.text = [[_lapTimerModel challengeAtIndex:indexPath.row] name];
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _lapTimerModel.challenges.count;
}

/* delegate Implementations */

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return @[@1, @2, @3];
}

-(IBAction)createNewChallenge:(id)sender
{
    UIAlertView *getNewChallenge = [[UIAlertView alloc] initWithTitle:@"New Challenge" message:@"Enter the name for this new challenge" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Create", nil];
    getNewChallenge.alertViewStyle = UIAlertViewStylePlainTextInput;

    [getNewChallenge show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 0)
        return;

    [_lapTimerModel addChallenge:[[alertView textFieldAtIndex:0] text]];
    [_challengesTableView reloadData];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
