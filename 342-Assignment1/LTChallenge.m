//
//  LTChallenge.m
//  342-Assignment1
//
//  Created by timeimp on 1/09/2014.
//  Copyright (c) 2014 James Wilson. All rights reserved.
//

#import "LTChallenge.h"

@interface LTChallenge()

@end

@implementation LTChallenge

-(NSString *)name
{
    return _challengeName;
}

-(NSArray *)times
{
    return _times;
}

@end
