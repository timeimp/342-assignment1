//
//  LTModel.m
//  342-Assignment1
//
//  Created by timeimp on 1/09/2014.
//  Copyright (c) 2014 James Wilson. All rights reserved.
//

#import "LTModel.h"

@interface LTModel()


@end


@implementation LTModel

-(id)init
{
    self = [super init];
    if(self){
        _challenges = [[NSMutableArray alloc] init];
    }

    NSLog(@"Created %lu challenges.", (unsigned long)_challenges.count);

    return self;
}

-(NSArray *)challenges
{
    return _challenges;
}

-(void) addChallenge: (NSString*) challengeName
{
    LTChallenge *newChallenge = [LTChallenge new];
    newChallenge.challengeName = challengeName;
    newChallenge.times = @[];
    [_challenges addObject:newChallenge];
}

-(LTChallenge *)challengeAtIndex: (NSInteger) index
{
    return [_challenges objectAtIndex:index];
}

-(int) numberOfChallenges
{
    return (int)_challenges.count;
}

@end
