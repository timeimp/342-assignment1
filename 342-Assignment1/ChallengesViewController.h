//
//  ChallengesViewController.h
//  342-Assignment1
//
//  Created by timeimp on 1/09/2014.
//  Copyright (c) 2014 James Wilson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LTModel.h"

@interface ChallengesViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *challengesTableView;

-(IBAction)createNewChallenge:(id)sender;

@property (nonatomic) LTModel *lapTimerModel;

@end

