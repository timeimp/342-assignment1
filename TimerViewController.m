//
//  TimerViewController.m
//  342-Assignment1
//
//  Created by timeimp on 1/09/2014.
//  Copyright (c) 2014 James Wilson. All rights reserved.
//

#import "TimerViewController.h"

@interface TimerViewController ()

@property NSTimer *counter;
@property enum buttonState timerButtonState;
@property NSDate* startDate;

@end

@implementation TimerViewController

- (void)viewDidLoad {

    [super viewDidLoad];

    [_currentProgressBar setFrame:CGRectMake(16.0,283.0,288.0,12.0)];
    _currentProgressBar.tintColor = [UIColor redColor];
    _currentProgressBar.tintColor = [UIColor whiteColor];
    [_currentProgressBar setProgress:0.7];

    _timerButtonState = startMsg;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)toggleButton:(id)sender
{
    switch(_timerButtonState)
    {
        case startMsg: {

            [_timerButton setTitle:@"Stop" forState:UIControlStateNormal];
            _startDate = [NSDate date];
            _counter = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(doTick:) userInfo:nil repeats:YES];

            _timerButtonState = stopMsg;
            break;
        }

        case stopMsg: {

            [_timerButton setTitle:@"Clear" forState:UIControlStateNormal];
            _timerButtonState = clearMsg;
            [_counter invalidate];

            UIAlertView *getNewChallenge = [[UIAlertView alloc] initWithTitle:@"Save Time" message:@"Enter a comment" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Create", nil];
            getNewChallenge.alertViewStyle = UIAlertViewStylePlainTextInput;


            [getNewChallenge show];
            break;
        }

        case clearMsg:[_timerButton setTitle:@"Start" forState:UIControlStateNormal];
            _timerButtonState = startMsg;
            break;
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 0)
        return;

    LTTime *newTime = [LTTime new];
    newTime.timeComment = [[alertView textFieldAtIndex:0] text];
    NSDate *currentDate = [[NSDate date] dateByAddingTimeInterval:0];

    newTime.timeValue = [currentDate timeIntervalSinceDate:_startDate];
    newTime.timeDate = currentDate;

}

-(void)doTick:(NSTimer *)timer
{

    NSDate *currentDate = [[NSDate date] dateByAddingTimeInterval:0];

    _timeLabel.text = [LTTime convertTimeToString:[currentDate timeIntervalSinceDate:_startDate]];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
